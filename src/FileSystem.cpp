/*
 * FileSystem.cpp
 *
 *  Created on: Jan 10, 2015
 *      Author: Rahul
 */

#include "FileSystem.hpp"

FileSystem* FileSystem::_file_system_manager = 0;

FileSystem* FileSystem::instance()
{
	if(!_file_system_manager)
	{
		_file_system_manager = new FileSystem();
	}

	return _file_system_manager;
}

FileSystem::FileSystem()
{
	this->iosys = new IOSystem();
	//this->read_data_buffer = malloc(BLOCK_SIZE);
	/* Init MASK values */
	this->init_masks();

}

FileSystem::~FileSystem()
{
	//free(this->read_data_buffer);
	delete iosys;
	delete _file_system_manager;
}

RC FileSystem::create(std::string filename)
{
	if (printDebug && createDebug)
	{
		std::cout << "------ ENTER CREATE ------" << std::endl;
	}

	if (filename.size() > MAX_FILENAME_SIZE)
	{
		return rc_filename_too_big;
	}

	std::vector<std::string> current_files;
	current_files = this->directory();

	if (printDebug && createDebug)
	{
		std::cout << "Files:  ";
		int size = current_files.size();
		for (int i = 0; i < size; ++i)
		{
			std::cout << current_files[i] << "  ";
		}
		std::cout << std::endl;
	}

	void* fname_chars = malloc(FILENAME_CHUNK);
	memset(fname_chars, '~', FILENAME_CHUNK);
	memcpy(fname_chars, filename.c_str(), filename.size());
	std::string filename_str = std::string((char*)fname_chars, FILENAME_CHUNK);
	free(fname_chars);

	int size = current_files.size();
	for (int i = 0; i < size; ++i)
	{
		if (filename_str == current_files[i])
		{
			return rc_filename_already_exists;
		}
	}

	/* Find free block in ldisk for first block of file, set bitmap for this block */
	int free_block = this->find_free_block_on_ldisk();
	this->set_bitmap(free_block);

	/* Memset allocated block as indication that it is allocated */
	void* temp_block = malloc(BLOCK_SIZE);
	memset(temp_block, '0', BLOCK_SIZE);
	this->iosys->write_block(free_block, (char*)temp_block);
	free(temp_block);

	/* Find free file descriptor index*/
	int free_fd_index = this->find_free_fd_index();

//	if (printDebug && createDebug)
//	{
//		std::cout << "Filename = " << filename << std::endl;
//		std::cout << "Free block = " << free_block << std::endl;
//		std::cout << "free_fd_index = " << free_fd_index << std::endl;
//		this->_oft->print_oft();
//	}

	void* fd_data = malloc(FILE_DESC_SIZE);
	this->create_fd_entry(fd_data, 0, free_block, -1, -1);
//	if (printDebug && createDebug)
//	{
//		std::cout << "Created file descriptor:" << std::endl;
//		this->print_fd_entry(free_fd_index, fd_data);
//	}

	this->set_fd_entry(free_fd_index, fd_data);

//	if (printDebug && createDebug)
//	{
//		int fd_disk_block = this->_oft->calc_fd_block_num(free_fd_index);
//		int fd_block_offset = this->_oft->calc_fd_block_offset(free_fd_index);
//		std::cout << "Printing in create: " << std::endl;
//		void* test_data = malloc(BLOCK_SIZE);
//		this->iosys->read_block(fd_disk_block, (char*)test_data);
//		//memset(test_data, '*', 16);
//		int byte_offset = fd_block_offset * FILE_DESC_SIZE;
//		this->print_fd_entry(free_fd_index, (void*)((char*)test_data + byte_offset));
//		this->iosys->write_block(fd_disk_block, (char*)test_data);
//		this->ldisk_export("test_create_after_test.txt");
//		free(test_data);
//	}
	free(fd_data);

	int free_dir_entry_index = this->find_free_directory_entry();

	if (printDebug && createDebug)
	{
		std::cout << "free_dir_entry_index = " << free_dir_entry_index << std::endl;
	}

	/* Modify directory file */
	void* dir_entry_data = malloc(DIR_ENTRY_SIZE);
	this->create_directory_entry(dir_entry_data, filename, free_fd_index);
	this->set_directory_entry(free_dir_entry_index, dir_entry_data);
	free(dir_entry_data);

//	if (printDebug && createDebug)
//	{
//		std::cout << "Printing after getting entry:" << std::endl;
//		void* test_data2 = malloc(BLOCK_SIZE);
//		void* entr = malloc(FILE_DESC_SIZE);
//		this->get_fd_entry(free_fd_index,entr);
//		this->print_fd_entry(free_fd_index, entr);
//		free(entr);
//		free(test_data2);
//	}

	if (printDebug && createDebug)
	{
		std::cout << "------ EXIT CREATE ------" << std::endl;
	}
	return rc_success;
}

RC FileSystem::destroy(std::string filename)
{
	if (printDebug && destroyDebug)
	{
		std::cout << "------ ENTER DESTROY------" << std::endl;
	}

	if (filename.size() > MAX_FILENAME_SIZE)
	{
		return rc_filename_too_big;
	}

	/* Find file in file directory */

	int fd_index = -1;
	void* fname_chars = malloc(FILENAME_CHUNK);
	memset(fname_chars, '~', FILENAME_CHUNK);
	memcpy(fname_chars, filename.c_str(), filename.size());
	void* dir_entry_data = malloc(DIR_ENTRY_SIZE);
	for (int i = 0; i < (DIR_ENTRIES_PER_BLOCK) * FILE_SIZE_BLOCKS; ++i)
	{
		this->get_directory_entry(i, dir_entry_data);
		int result = memcmp(fname_chars, dir_entry_data, FILENAME_CHUNK);
		if (result == 0)
		{
			int offset = FILENAME_CHUNK;
			fd_index = *(int*)((char*)dir_entry_data + offset);

			/* Check to see if this file is in the OFT */
			for (int j = 0; j < OFT_TABLE_SIZE; ++j)
			{
				if (this->_oft->fd_index[i] == fd_index)
				{
					fd_index = -2;
					break;
				}
			}

			/* Free file directory entry */
			memset( (char*)dir_entry_data, '~', FILENAME_CHUNK);
			*(int*)((char*)dir_entry_data + offset) = -1;
			this->set_directory_entry(i, dir_entry_data);
			break;
		}
	}
	free(fname_chars);
	free(dir_entry_data);

	if (fd_index == -1)
	{
		return rc_file_does_not_exist;
	}
	else if (fd_index == -2)
	{
		return rc_trying_to_delete_open_file;
	}

	/* Modify bitmap accordingly, and clear file directory slot */

	void* temp_block_data = malloc(BLOCK_SIZE);
	memset(temp_block_data, '%', BLOCK_SIZE);
	for (int i = 0; i < FILE_SIZE_BLOCKS; ++i)
	{


		int ldisk_block_num = this->get_fd_value(fd_index, i + 1);
		if (ldisk_block_num != -1)
		{
			this->clr_bitmap(ldisk_block_num);
			this->set_fd_value(fd_index, i + 1, -1);
			this->iosys->write_block(ldisk_block_num, (char*)temp_block_data);
		}

	}
	free(temp_block_data);
	this->set_fd_value(fd_index, 0, -1); // Set file length to -1

	if (printDebug && destroyDebug)
	{
		std::cout << "------ EXIT DESTROY------" << std::endl;
	}

	return rc_success;
}

int FileSystem::open(std::string filename)
{
	if (printDebug && openDebug)
	{
		std::cout << "------ ENTER OPEN------" << std::endl;
		this->_oft->print_oft();
	}

	/* Get fd_index of file */

	int fd_index = -1;
	void* fname_chars = malloc(FILENAME_CHUNK);
	memset(fname_chars, '~', FILENAME_CHUNK);
	memcpy(fname_chars, filename.c_str(), filename.size());
	void* dir_entry_data = malloc(DIR_ENTRY_SIZE);
	for (int i = 0; i < (DIR_ENTRIES_PER_BLOCK) * FILE_SIZE_BLOCKS; ++i)
	{
		this->get_directory_entry(i, dir_entry_data);
		int result = memcmp(fname_chars, dir_entry_data, FILENAME_CHUNK);
		if (result == 0)
		{
			int offset = FILENAME_CHUNK;
			fd_index = *(int*)((char*)dir_entry_data + offset);
			if (printDebug && openDebug)
			{
				std::cout << "memcmp filename result = " << result << "\t\tfd_index = " << fd_index << std::endl;
			}
			break;
		}
	}
	free(fname_chars);
	free(dir_entry_data);

	if (fd_index == -1)
	{
		//std::cout <<  "\n\n\t\t---------------\tERROR:  CANNOT OPEN. FILE DOES NOT EXIST \n" << std::endl;
		return rc_file_does_not_exist;
	}

	/* Set up open file table */

	int free_oft_index = this->_oft->find_free_oft_index();
	if ( free_oft_index == -1 )
	{
//		std::cout <<  "\t-----------ERROR:  OPEN FILE TABLE IS FULL------------" << std::flush;
		return rc_oft_full;
	}

	this->_oft->curr_pos[free_oft_index] = BLOCK_SIZE * FILE_SIZE_BLOCKS - 1; // Dummy positioning
	this->_oft->fd_index[free_oft_index] = fd_index;
	int file_length = this->get_fd_value(fd_index, 0);
	this->_oft->file_length[free_oft_index] = file_length;
	this->lseek(free_oft_index, 0); // Seek to beginning of file. lseek will bring block into buffer because we set the dummy curr_pos to the last byte in the file

	if (printDebug && openDebug)
	{
		this->_oft->print_oft();
		std::cout << "------ EXIT OPEN------" << std::endl;
	}

	return free_oft_index;
}

RC FileSystem::close(int oft_index)
{
	if (printDebug && closeDebug)
	{
		std::cout << "------ ENTER CLOSE------" << std::endl;
		this->_oft->print_oft();
	}

	/* Updating file length on ldisk */
	this->set_fd_value(this->_oft->fd_index[oft_index], 0, this->_oft->file_length[oft_index]);

	/* Freeing oft entry */
	this->_oft->curr_pos[oft_index] = -1;
	this->_oft->fd_index[oft_index] = -1;
	this->_oft->file_length[oft_index] = -1;
	memset(this->_oft->oft_buffers[oft_index], '&', BLOCK_SIZE);

	if (printDebug && closeDebug)
	{
		std::cout << "------ EXIT CLOSE------" << std::endl;
		this->_oft->print_oft();
	}

	return rc_success;
}

int FileSystem::read(int oft_index, void* read_data, int count)
{
	bool trueCountStatus = true;

	if (printDebug && readDebug)
	{
		std::cout << "\n\n------ ENTER READ ------" << std::endl;
		std::cout << "oft_index = " << oft_index << std::endl;
		this->_oft->print_oft();
	}

	int current_file_byte_num = this->_oft->curr_pos[oft_index];
	int current_file_block_num = this->calc_file_block_num(current_file_byte_num);
	int end_file_byte_num = current_file_byte_num + count - 1;
	int end_file_block_num = this->calc_file_block_num(end_file_byte_num);

	/* Adjusting end_file_byte_num and end_file_block_num if read request goes beyond file size */
	int true_count = count;
	if (end_file_byte_num < this->_oft->file_length[oft_index])
	{	}
	else
	{
		if (trueCountStatus == true)
		{
			true_count = this->_oft->file_length[oft_index] - current_file_byte_num;
			end_file_byte_num = this->_oft->file_length[oft_index] - 1;
			end_file_block_num = this->calc_file_block_num(end_file_byte_num);
		}
	}

	/* Some error checks */
	if (this->_oft->fd_index[oft_index] < 0)
	{
		return 0; // 0 bytes written.
	}
	if (current_file_byte_num < 0)
	{
		//std::cout << "\nERROR: Invalid read request" << std::endl;
		//assert(current_file_byte_num >= 0);
		return 0; // 0 bytes read
	}

//	if (current_block_num == end_block_num)
//	{
//		//std::cout << "INSIDE EQUALITY" << std::endl;
//		memcpy(read_data, (char*)this->_oft->oft_buffers[oft_index] + current_byte_num /* %64 */, true_count);
//	}

	/* Read loop. Handles reads that span multiple blocks */

	bool isReadDone = false;
	int read_offset = 0;
	while (isReadDone == false)
	{
		//current_byte_num = this->_oft->curr_pos[oft_index];
		//current_block_num = this->calc_file_block_num(current_byte_num);
		//end_byte_num = current_byte_num + true_count - 1;
		//end_block_num = this->calc_file_block_num(end_byte_num);
		if (printDebug && readDebug)
		{
			this->_oft->print_oft();
		}

		if (current_file_block_num != end_file_block_num)
		{
			/* Read till end of block*/
			int to_read = BLOCK_SIZE - current_file_byte_num % BLOCK_SIZE;
			memcpy((char*)read_data + read_offset, (char*)this->_oft->oft_buffers[oft_index] + current_file_byte_num % BLOCK_SIZE, to_read);
			read_offset += to_read;

			if (printDebug && readDebug)
			{
				std::cout << "\nReached different block case" << std::endl;
				std::cout << "to_read = " << to_read << std::endl;
			}

			/* Bring in next block and update OFT parameters */
			current_file_byte_num = current_file_byte_num + to_read;
			current_file_block_num = current_file_block_num + 1;
			this->_oft->curr_pos[oft_index] = current_file_byte_num;
			int current_disk_block_num = this->_oft->get_disk_block_num_of_file(oft_index, current_file_block_num);
			this->iosys->read_block(current_disk_block_num, (char*)this->_oft->oft_buffers[oft_index]);
			if (printDebug && readDebug)
			{
				std::cout << "current_file_byte_num = " << current_file_byte_num << "\tcurrent_file_block_num = " << current_file_block_num << std::endl;
			}

		}
		else
		{
			int to_read = (end_file_byte_num /* + 1 */) % BLOCK_SIZE - current_file_byte_num % BLOCK_SIZE + 1;
			if (printDebug && readDebug)
			{
				std::cout << "\nReached same block case" << std::endl;
				std::cout << "to_read = " << to_read << std::endl;
			}
			memcpy((char*)read_data + read_offset, (char*)this->_oft->oft_buffers[oft_index] + current_file_byte_num % BLOCK_SIZE, to_read);
			isReadDone = true;
			current_file_byte_num = current_file_byte_num + to_read;

			if (printDebug && readDebug)
			{
				std::cout << "\tdebug: current_file_byte_num = " << current_file_byte_num << std::endl;
			}
			if (current_file_byte_num % BLOCK_SIZE == 0 && current_file_byte_num < BLOCK_SIZE*FILE_SIZE_BLOCKS)
			{
				this->lseek(oft_index, current_file_byte_num);
			}
			this->_oft->curr_pos[oft_index] = current_file_byte_num;
		}
	}


	if (printDebug && readDebug)
	{
		std::cout << "oft_index = " << oft_index << "\tcount = " << count << "\ttrue_count = " << true_count << std::endl;
		this->_oft->print_oft();
		this->iosys->print_block_as_string((char*)read_data);
		std::cout << "------ EXIT READ ------" << std::endl;
	}

	return true_count;
}

int FileSystem::write(int oft_index, void* write_data, int count)
{
	if (printDebug && writeDebug)
		std::cout << "\n\n------ ENTER WRITE ------" << std::endl;

	int current_file_byte_num = this->_oft->curr_pos[oft_index];
	int current_file_block_num = this->calc_file_block_num(current_file_byte_num);
	int end_file_byte_num = current_file_byte_num + count - 1;

	/* Corner case corrections / error messages*/
	if (this->_oft->fd_index[oft_index] < 0)
	{
		return 0; // 0 bytes written.
	}
	if (end_file_byte_num >= FILE_SIZE_BLOCKS * BLOCK_SIZE)
	{
		end_file_byte_num = FILE_SIZE_BLOCKS * BLOCK_SIZE - 1;
	}
	if( current_file_byte_num > end_file_byte_num)
	{
		/*std::cout <<  "\n\n\t\t---------------\tERROR:  current_file_byte_num > end_file_byte_num\n" << std::endl;*/
		return rc_invalid_write_request;
	}
	if ( current_file_byte_num >= FILE_SIZE_BLOCKS * BLOCK_SIZE)
	{
//		std::cout <<  "\n\n\t\t---------------\tERROR:  current_file_byte_num >= FILE_SIZE_BLOCKS * BLOCK_SIZE\n" << std::endl;
		return rc_invalid_write_request;
	}
	int true_count = (end_file_byte_num + 1) - current_file_byte_num;
	int end_file_block_num = this->calc_file_block_num(end_file_byte_num);

	// TODO check file allocation for errors
	int temp_pos_byte_num = end_file_byte_num + 1;
	int temp_pos_block_num = this->calc_file_block_num(temp_pos_byte_num);

	if (temp_pos_block_num - current_file_block_num > 0)
	{
		int diff = temp_pos_block_num - current_file_block_num;
		if (printDebug && writeDebug)
		{
			std::cout << "!!! REACHED ALLOCATE !!!" << std::endl;
			std::cout << "diff = " << diff << std::endl;
		}

		/* In the case the file has reached the maximum size, don't allocate one more block */
		if (diff == FILE_SIZE_BLOCKS)
		{
			diff = diff - 1;
			if (printDebug && writeDebug)
			{
				std::cout << "reached truncated diff = " << diff << std::endl;
			}
		}

		for (int i = 0; i < diff; ++i)
		{
			this->allocate_block(this->_oft->fd_index[oft_index]);
		}
	}


//	/* Simple write */
//	if (current_file_block_num == end_file_block_num)
//	{
//		std::cout << "\t\tINSIDE EQUALITY WRITE" << std::endl;
//		memcpy((char*)this->_oft->oft_buffers[oft_index] + current_file_byte_num % 64, write_data, count);
//		int disk_block_num = this->_oft->calc_disk_block_num_of_file(oft_index, current_file_block_num);
//		this->iosys->write_block(disk_block_num, (char*)this->_oft->oft_buffers[oft_index]);
//		this->_oft->curr_pos[oft_index] += count;
//	}

	/* Write loop*/

	bool isWriteDone = false;
	int write_offset = 0;
	while (isWriteDone == false)
	{

		if (printDebug && writeDebug)
			this->_oft->print_oft();

		if (current_file_block_num != end_file_block_num)
		{
			/* Write till end of block */
			int to_write = BLOCK_SIZE - current_file_byte_num % BLOCK_SIZE;
			memcpy((char*)this->_oft->oft_buffers[oft_index] + current_file_byte_num % BLOCK_SIZE, (char*)write_data + write_offset, to_write);
			write_offset += to_write;

			if (printDebug && writeDebug)
			{
				std::cout << "\nReached different block case WRITE" << std::endl;
				std::cout << "to_write = " << to_write << std::endl;
			}

			/* Write current block to disk, update OFT parameters, and bring in next block to continue writing */
			int current_disk_block_num = this->_oft->get_disk_block_num_of_file(oft_index, current_file_block_num);
			this->iosys->write_block(current_disk_block_num, (char*)this->_oft->oft_buffers[oft_index]);

			if (printDebug && writeDebug)
			{
				std::cout << "current_file_byte_num = " << current_file_byte_num << "\tcurrent_file_block_num = " << current_file_block_num << "\tcurrent_disk_block_num = " << current_disk_block_num << std::endl;
			}

			current_file_byte_num = current_file_byte_num + to_write;
			current_file_block_num = current_file_block_num + 1;
			this->_oft->curr_pos[oft_index] = current_file_byte_num;
			//TODO update file length

			if (printDebug && writeDebug)
			{
				std::cout << "---REACHED--- " << __LINE__ << std::endl;
				this->_oft->print_oft();
//				this->ldisk_export("write_test_1.txt");
				std::cout << "---REACHED--- " << __LINE__ << std::endl;
			}

			current_disk_block_num = this->_oft->get_disk_block_num_of_file(oft_index, current_file_block_num);
			if (printDebug && writeDebug)
			{
				std::cout << "oft_index = " << oft_index << std::endl;
				std::cout << "\"current_file_byte_num\" = " << current_file_byte_num << "\t\"current_file_block_num\" = " << current_file_block_num << "\t\"current_disk_block_num\" = " << current_disk_block_num << std::endl;
			}
			this->iosys->read_block(current_disk_block_num, (char*)this->_oft->oft_buffers[oft_index]);



		}
		else
		{
			int to_write = (end_file_byte_num) % BLOCK_SIZE - current_file_byte_num % BLOCK_SIZE + 1;
			if (printDebug && writeDebug)
			{
				std::cout << "\nReached same block case	WRITE" << std::endl;
				std::cout << "to_write = " << to_write << std::endl;
			}


			memcpy((char*)this->_oft->oft_buffers[oft_index] + current_file_byte_num % BLOCK_SIZE, (char*)write_data + write_offset, to_write);
			int current_disk_block_num = this->_oft->get_disk_block_num_of_file(oft_index, current_file_block_num);
			this->iosys->write_block(current_disk_block_num, (char*)this->_oft->oft_buffers[oft_index]);
			isWriteDone = true;


			current_file_byte_num = current_file_byte_num + to_write;
			this->_oft->curr_pos[oft_index] = current_file_byte_num;
			this->_oft->file_length[oft_index] = this->_oft->file_length[oft_index] > end_file_byte_num  ? this->_oft->file_length[oft_index] : end_file_byte_num + 1;
			this->set_fd_value(this->_oft->fd_index[oft_index], 0, this->_oft->file_length[oft_index]);

			// TODO verify that this goes here
			if (printDebug && writeDebug)
			{
				std::cout << "\n\tdebug: current_file_byte_num = " << current_file_byte_num << std::endl;
			}
			if (current_file_byte_num % BLOCK_SIZE == 0 && current_file_byte_num < BLOCK_SIZE*FILE_SIZE_BLOCKS)
			{
				std::cout << "---REACHED--- " << __LINE__ << std::endl;
				this->lseek(oft_index, current_file_byte_num);
				std::cout << "---REACHED--- " << __LINE__ << std::endl;
			}
		}
	}


	if (printDebug && writeDebug)
	{
		std::cout << "oft_index = " << oft_index << "\tcount = " << count << "\ttrue_count = " << true_count << std::endl;
		this->_oft->print_oft();
		std::cout << "------ EXIT WRITE ------\n" << std::endl;
	}

	return true_count;
}

RC FileSystem::lseek(int oft_index, int pos){

	if (printDebug && lseekDebug)
	{
		std::cout << "\n\n------ ENTER LSEEK ------" << std::endl;
		this->_oft->print_oft();
	}

	int current_block_num = this->calc_file_block_num(this->_oft->curr_pos[oft_index]);
	int desired_block_num = this->calc_file_block_num(pos);

	if (desired_block_num >= FILE_SIZE_BLOCKS)
	{
//		std::cout << "-------------ERROR: FileSystem::lseek - INVALID SEEK REQUEST" << std::endl;
		return rc_invalid_seek_request;
	}
	if (this->_oft->fd_index[oft_index] < 0)
	{
		return rc_invalid_seek_request;
	}

	if (printDebug && lseekDebug)
	{
		std::cout << "oft_index = " << oft_index << "\tcurrent_block_num = " << current_block_num << "\tdesired_block_num = " << desired_block_num << std::endl;
	}

	if (current_block_num != desired_block_num)
	{
		this->_oft->read_block_into_buffer(desired_block_num, oft_index);
	}

	this->_oft->set_current_pos(oft_index, pos);
	if (this->_oft->file_length[oft_index] < pos )
	{
		this->_oft->file_length[oft_index] = pos;
	}

	if (printDebug && lseekDebug)
		std::cout << "\n\n------ EXIT LSEEK ------" << std::endl;

	return rc_success;
}

std::vector<std::string> FileSystem::directory()
{
	std::vector<std::string> names;
	int fd_index = -1;
	void* fname_chars = malloc(FILENAME_CHUNK);
	void* dir_entry_data = malloc(DIR_ENTRY_SIZE);
	for (int i = 0; i < (DIR_ENTRIES_PER_BLOCK) * FILE_SIZE_BLOCKS; ++i)
	{
		this->get_directory_entry(i, dir_entry_data);
		int offset = 0;
		std::string filename = std::string((char*)dir_entry_data + offset, FILENAME_CHUNK);
		offset += FILENAME_CHUNK;
		int temp_fd_index = *(int*)((char*)dir_entry_data + offset);
		if (printDebug && dirDebug)
		{
			std::cout << "temp_fd_index = " << temp_fd_index << std::endl;
		}
		if (temp_fd_index != -1)
		{
			names.push_back(filename);
		}
	}
	free(fname_chars);
	free(dir_entry_data);
	return names;
}

RC FileSystem::init()
{
	void* data = malloc(BLOCK_SIZE);

	/* ++++++++++++++++++++++++++++++++++++++++++++++ */
	/* Setting bitmap indicating that first 8 blocks are taken by BM, 6xFD, 1st block dir*/

	this->iosys->read_block(0, (char*)data);
	if (printDebug && fsInitDebug)
		this->iosys->print_block_as_string((char*)data);

	unsigned int quo = NUM_BLOCKS/(sizeof(int)*8);
	unsigned int mod = NUM_BLOCKS%(sizeof(int)*8);
	int num_ints = 0;
	if (mod == 0)
		num_ints = quo;
	else
		num_ints = quo + 1;

	for (int j = 0; j < num_ints; ++j)
	{
		for (int i = 0; i < 32; ++i)
		{
			this->clr_bit( *((int*)data + j), i );
		}
	}

	for (int i = 0; i < INIT_BITMAP_ALLOCATION; ++i)
	{
		this->set_bit( *(int*)data, i );
	}
	if (printDebug && fsInitDebug)
		this->iosys->print_block_as_string((char*)data);

	this->iosys->write_block(0, (char*)data);

	/* ++++++++++++++++++++++++++++++++++++++++++++++ */
	/* Initializing the File Descriptor Blocks*/

	this->init_file_descriptor_block(data);
	for (int i = FIRST_FD_BLOCK; i < LAST_FD_BLOCK + 1; ++i)
	{
		this->iosys->write_block(i, (char*)data);
	}

	/* ++++++++++++++++++++++++++++++++++++++++++++++ */
	/* Initializing the File Descriptor of the Directory File*/

	this->iosys->read_block(1, (char*)data);
	if (printDebug && fsInitDebug)
		this->iosys->print_block_as_string((char*)data);

	int int_offset = 0;
	*((int*)data + int_offset) = 192; // Initial length of directory file in bytes
	++int_offset;
	*((int*)data + int_offset) = 7; // Directory file starts just after BM + 6xFD
	++int_offset;
	*((int*)data + int_offset) = 8; // Dummy value indicating file small enough not to need the block this would point to
	++int_offset;
	*((int*)data + int_offset) = 9; // " "
	++int_offset;

	this->iosys->write_block(1, (char*)data);
	if (printDebug && fsInitDebug)
		this->iosys->print_block_as_string((char*)data);

	/* ++++++++++++++++++++++++++++++++++++++++++++++ */
	/* Initializing Directory file and adding entry to reference itself */

	for (int i = FIRST_DIRECTORY_BLOCK; i < LAST_DIRECTORY_BLOCK + 1; ++i)
	{
		this->iosys->read_block(i, (char*)data);
		this->init_directory_block(data);
		this->iosys->write_block(i, (char*)data);
	}

	this->iosys->read_block(7, (char*)data);
	if (printDebug && fsInitDebug)
		this->iosys->print_block_as_string((char*)data);

	this->init_directory_block(data);

	std::string dir_name = "dir";
	int fd_ind = 0;

	int char_offset = 0;
	memcpy((char*)data + char_offset, dir_name.c_str(), dir_name.size());
	char_offset += 4;
	*(int*)((char*)data + char_offset) = fd_ind;
	char_offset += 4;

	this->iosys->write_block(7, (char*)data);
	if (printDebug && fsInitDebug)
		this->iosys->print_block_as_string((char*)data);

	/* Initialize OpenFileTable */
	if (this->_oft != NULL)
	{
		delete this->_oft;
	}
	this->_oft = new OpenFileTable(this->_file_system_manager);
	this->_oft->open_directory();

	free(data);
	return rc_success;
}

RC FileSystem::init(std::string disk_name)
{
	RC rc = this->iosys->import_disk(disk_name);
	if (rc != rc_success)
	{
		return rc;
	}

	this->_oft = new OpenFileTable(this->_file_system_manager);
	this->_oft->open_directory();

	return rc;
}

RC FileSystem::save(std::string disk_name)
{
	delete this->_oft;
	RC rc = this->iosys->export_disk(disk_name);

	void* temp_block = malloc(BLOCK_SIZE);
	memset(temp_block, '=', BLOCK_SIZE);
	for(int i = 0; i < NUM_BLOCKS; ++i)
	{
		this->iosys->write_block(i, (char*)temp_block);
	}
	free(temp_block);

	return rc;
}

void FileSystem::ldisk_export(std::string disk_name)
{
	this->iosys->export_disk(disk_name);
}

void FileSystem::init_masks()
{
	unsigned int mask_temp = 0x80000000;
	for (unsigned i = 0; i < sizeof(mask_size) * 8; ++i)
	{
		this->MASK[i] = mask_temp;
		this->MASK2[i] = ~mask_temp;
		mask_temp = mask_temp >> 1;
	}

	if (printDebug && maskDebug)
	{
		std::cout << "\tMASK" << std::endl;
		for (unsigned i = 0; i < sizeof(mask_size) * 8; ++i)
		{
			std::cout << (std::bitset<sizeof(mask_size)*8>) MASK[i] << std::endl;
		}
		std::cout << "\n\tMASK2" << std::endl;
		for (unsigned i = 0; i < sizeof(mask_size) * 8; ++i)
		{
			std::cout << (std::bitset<sizeof(mask_size)*8>) MASK2[i] << std::endl;
		}
	}
}

void FileSystem::set_bit(int &num, int i)
{
	num |= MASK[i];
}

void FileSystem::clr_bit(int &num, int i)
{
	num &= MASK2[i];
}

int FileSystem::get_bit(const int &num, int i)
{
	int val = num & MASK[i];
	return val==0 ? 0 : 1;
}

void FileSystem::set_bitmap(int block_num)
{
	void* bitmap_block_data = malloc(BLOCK_SIZE);
	this->iosys->read_block(0, (char*)bitmap_block_data);
	int int_num = block_num / (sizeof(int) * 8);
	this->set_bit(*((int*)bitmap_block_data + int_num), block_num % (sizeof(int) * 8));
	this->iosys->write_block(0, (char*)bitmap_block_data);
	free(bitmap_block_data);
}

void FileSystem::clr_bitmap(int block_num)
{
	void* bitmap_block_data = malloc(BLOCK_SIZE);
	this->iosys->read_block(0, (char*)bitmap_block_data);
	int int_num = block_num / (sizeof(int) * 8);
	this->clr_bit(*((int*)bitmap_block_data + int_num), block_num % (sizeof(int) * 8));
	this->iosys->write_block(0, (char*)bitmap_block_data);
	free(bitmap_block_data);
}

void FileSystem::init_file_descriptor_block(void* fd_data)
{
	int int_offset = 0;
	int num_fd_entries_block = BLOCK_SIZE / FILE_DESC_SIZE;
	for (int i = 0; i < num_fd_entries_block; ++i)
	{
		*((int*)fd_data + int_offset) = -1; // Initialize to -1 so that create/destroy scans can compare against this
		++int_offset;
		*((int*)fd_data + int_offset) = -1; //
		++int_offset;
		*((int*)fd_data + int_offset) = -1; //
		++int_offset;
		*((int*)fd_data + int_offset) = -1; //
		++int_offset;
	}
}

void FileSystem::init_directory_block(void* dir_data)
{
	int fd_ind = -1;

	int char_offset = 0;
	char_offset += 4;
	int num_dir_entries_block = BLOCK_SIZE/DIR_ENTRY_SIZE;
	for (int i = 0; i < num_dir_entries_block; ++i)
	{
		*(int*)((char*)dir_data + char_offset) = fd_ind;
		char_offset += DIR_ENTRY_SIZE;
	}
}

int FileSystem::calc_file_block_num(int file_pos)
{
	return file_pos / BLOCK_SIZE;
}

int FileSystem::find_free_block_on_ldisk()
{
	int free_file_block = -1;
	void* data = malloc(BLOCK_SIZE);
	this->iosys->read_block(0, (char*)data);

//	int num_ints = this->calc_num_ints_bitmap(NUM_BLOCKS);
	int num_ints = BITMAP_NUM_INTS;
	for (int j = 0; j < num_ints; ++j)
	{
		for (int i = 0; i < 32; ++i)
		{
			int isBlockUsed = this->get_bit( *((int*)data + j), i );
			int block_num = sizeof(int) * 8 * j + i;
			if (isBlockUsed == 0 && block_num > LAST_DIRECTORY_BLOCK)
			{
				free_file_block = block_num;
				break;
			}
		}
		if(free_file_block != -1)
		{
			break;
		}
	}

	free(data);
	assert(free_file_block >= 0 && free_file_block < 64);
	return free_file_block;
}

int FileSystem::find_free_fd_index()
{
	void* desc_data = malloc(BLOCK_SIZE);
	int free_fd_index = -1;
	for (int i = FIRST_FD_BLOCK; i < LAST_FD_BLOCK + 1; ++i)
	{
		this->iosys->read_block(i, (char*)desc_data);
		int fd_offset = 0;
		for (int j = 0; j < FDS_PER_BLOCK; ++j)
		{
			int file_length_value = *(int*)((char*)desc_data + fd_offset);
			fd_offset += FILE_DESC_SIZE;
			if(file_length_value == -1)
			{
				free_fd_index = (i - 1)*FDS_PER_BLOCK + j;
				break;
			}
			if(printDebug && createDebug)
			{
				std::cout << "file_length_value = " << file_length_value << std::endl;
				std::cout << "free_fd_index = " << free_fd_index << std::endl;
			}
		}
		if (free_fd_index != -1)
		{
			break;
		}

	}

	free(desc_data);
	return free_fd_index;
}

void FileSystem::create_fd_entry(void* fd_data, int file_length, int disk_block_index_0, int disk_block_index_1, int disk_block_index_2)
{
	int int_offset = 0;
	*((int*)fd_data + int_offset) = file_length; //
	++int_offset;
	*((int*)fd_data + int_offset) = disk_block_index_0; //
	++int_offset;
	*((int*)fd_data + int_offset) = disk_block_index_1; //
	++int_offset;
	*((int*)fd_data + int_offset) = disk_block_index_2; //
	++int_offset;
}

void FileSystem::set_fd_entry(int fd_index, void* fd_data){

	int fd_disk_block = this->_oft->calc_fd_block_num(fd_index);
	int fd_block_offset = this->_oft->calc_fd_block_offset(fd_index);
	if (printDebug && createDebug)
	{
		std::cout << "fd_disk_block = " << fd_disk_block << "\tfd_block_offset = " << fd_block_offset << std::endl;
	}
	void* fd_block_data = malloc(BLOCK_SIZE);
	this->iosys->read_block(fd_disk_block, (char*)fd_block_data);
//	std::cout << "fd_data = " << fd_data << std::endl;
	//memset(fd_data, '%', FILE_DESC_SIZE);
//	std::cout << "fd_data = " << fd_data << std::endl;
	int byte_offset = fd_block_offset * FILE_DESC_SIZE;
//	std::cout << "byte_offset = " << fd_block_offset * FILE_DESC_SIZE << std::endl;
	memcpy((void*)((char*)fd_block_data + byte_offset), fd_data, FILE_DESC_SIZE);
//	std::cout << "Inside create fd_data:" << std::endl;
//	this->print_fd_entry(fd_index, (void*)((char*)fd_block_data + byte_offset));
	this->iosys->write_block(fd_disk_block, (char*)fd_block_data);

	if (printDebug && createDebug)
	{
		std::cout << "Printing after write and read" << std::endl;
		void* test_data = malloc(BLOCK_SIZE);
		this->iosys->read_block(fd_disk_block, (char*)test_data);
		//memset(test_data, '*', 16);
		this->print_fd_entry(fd_index, (void*)((char*)test_data + byte_offset));
		this->iosys->write_block(fd_disk_block, (char*)test_data);
		this->ldisk_export("test_create_after_test.txt");
		free(test_data);
	}



	free(fd_block_data);
}

void FileSystem::get_fd_entry(int fd_index, void* fd_data)
{

	int fd_disk_block = this->_oft->calc_fd_block_num(fd_index);
	int fd_block_offset = this->_oft->calc_fd_block_offset(fd_index);
	void* temp = malloc(BLOCK_SIZE);
	this->iosys->read_block(fd_disk_block, (char*)temp);
	int byte_offset = fd_block_offset * FILE_DESC_SIZE;
	memcpy(fd_data, (void*)((char*)temp + byte_offset), FILE_DESC_SIZE);
	free(temp);

}

int FileSystem::get_fd_value(int fd_index, int int_index)
{
	if (int_index >= (int)(FILE_DESC_SIZE/sizeof(int)) )
	{
		std::cout << "invalid index at line: " << __LINE__ << std::endl;
		assert(0==1);
	}
	void* fd_data = malloc(FILE_DESC_SIZE);
	this->get_fd_entry(fd_index, fd_data);
	int to_return = *((int*)fd_data + int_index);
	free(fd_data);

	return to_return;
}

void FileSystem::set_fd_value(int fd_index, int int_index, int value)
{
	if (int_index >= (int)(FILE_DESC_SIZE/sizeof(int)) )
	{
		std::cout << "invalid index at line: " << __LINE__ << std::endl;
		assert(0==1);
	}

	void* fd_data = malloc(FILE_DESC_SIZE);
	this->get_fd_entry(fd_index, fd_data);
	*((int*)fd_data + int_index) = value;
	this->set_fd_entry(fd_index, fd_data);
	free(fd_data);
}

void FileSystem::print_fd_entry(int fd_index, void* fd_data)
{
	std::cout << "\nFile Descriptor Entry: " << fd_index << std::endl;
	std::cout << "file_len\t" << "bl_ind_0\t" << "bl_ind_1\t" << "bl_ind_2" << std::endl;

	int int_offset = 0;
	std::cout << *((int*)fd_data + int_offset) << "\t\t"; // file_length
	++int_offset;
	std::cout << *((int*)fd_data + int_offset) << "\t\t"; // disk_block_index_0
	++int_offset;
	std::cout << *((int*)fd_data + int_offset) << "\t\t"; // disk_block_index_1
	++int_offset;
	std::cout << *((int*)fd_data + int_offset) << "\t\t" << std::endl; // disk_block_index_2
	++int_offset;

}

RC FileSystem::allocate_block(int fd_index)
{
	bool successful_alloc = false;

	void* fd_data = malloc(FILE_DESC_SIZE);
	this->get_fd_entry(fd_index, fd_data);
	int offset = 0;
	offset += sizeof(int);
	for (int i = 0; i < FILE_SIZE_BLOCKS; ++i)
	{
		int disk_block_num = *(int*)((char*)fd_data + offset);

		/* If file size is less than max file size*/
		if (disk_block_num == -1)
		{
			int free_block = this->find_free_block_on_ldisk();
			this->set_bitmap(free_block);
			*(int*)((char*)fd_data + offset) = free_block;
			this->set_fd_entry(fd_index, fd_data);

			/* Memset allocated block as indication that it is allocated */
			void* temp_block = malloc(BLOCK_SIZE);
			memset(temp_block, '0', BLOCK_SIZE);
			this->iosys->write_block(free_block, (char*)temp_block);
			free(temp_block);

			successful_alloc = true;
			break;
		}

		offset += sizeof(int); // must be done after if statement
	}

	free(fd_data);

	if (successful_alloc == true)
	{
		return rc_success;
	}
	else
	{
		//std::cout <<  "\n\n\t\t---------------\tERROR:  BLOCK ALLOCATION FAILED\n" << std::endl;
		return rc_failed_alloc;
	}
}

int FileSystem::find_free_directory_entry()
{
	void* dir_entry_data = malloc(DIR_ENTRY_SIZE);

	if (printDebug && createDebug)
	{
		this->_oft->print_oft();
	}

	/* Seek to beginning of directory */
	this->lseek(OFT_DIR, 0);

	/* Iterate through directory to find free index*/

	int dir_index_val;
	int free_dir_index = -99;
	int offset = 0;
	for (int i = 0; i < DIR_ENTRIES_PER_BLOCK * FILE_SIZE_BLOCKS; ++i)
	{
		this->read(OFT_DIR, dir_entry_data, DIR_ENTRY_SIZE);
		offset += DIR_ENTRY_SIZE;

		dir_index_val = *(int*)((char*)dir_entry_data + 4); // 4 for file_name size
//		std::cout << "Directory entry " << i << "\t\t%%%%%%%%%%%%%%%%%%%%%% DIR_INDEX = " << dir_index_val << std::endl;

		if (dir_index_val == -1)
		{
			free_dir_index = i;
			break;
		}
	}

	if (printDebug && createDebug)
	{
		this->_oft->print_oft();
	}

	free(dir_entry_data);
	return free_dir_index;
}

void FileSystem::create_directory_entry(void* dir_entry_data, std::string filename, int fd_index)
{
	memset(dir_entry_data, '~', DIR_ENTRY_SIZE);
	int char_offset = 0;
	memcpy((char*)dir_entry_data + char_offset, filename.c_str(), filename.size());
	char_offset += 4;
	*(int*)((char*)dir_entry_data + char_offset) = fd_index;
	char_offset += 4;
}

void FileSystem::set_directory_entry(int dir_entry_index, void* dir_entry_data)
{
	int directory_file_offset = 0;
	directory_file_offset += dir_entry_index * DIR_ENTRY_SIZE;
	this->lseek(OFT_DIR, directory_file_offset);
	this->write(OFT_DIR, dir_entry_data, DIR_ENTRY_SIZE);
}

void FileSystem::get_directory_entry(int dir_entry_index, void* dir_entry_data)
{
	int directory_file_offset = 0;
	directory_file_offset += dir_entry_index * DIR_ENTRY_SIZE;
	this->lseek(OFT_DIR, directory_file_offset);
	this->read(OFT_DIR, dir_entry_data, DIR_ENTRY_SIZE);
}
//void print_directory_entry(int dir_entry_index, void* dir_entry_data);

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* Open File Table */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

FileSystem::OpenFileTable::OpenFileTable(FileSystem* _file_system_manager) : _fsm(_file_system_manager)
{
	void* temp_buffer_ptr;
	for (int i = 0; i < OFT_TABLE_SIZE; ++i)
	{
		temp_buffer_ptr = malloc(BLOCK_SIZE);
		memset(temp_buffer_ptr, '$', BLOCK_SIZE);
		this->oft_buffers.push_back(temp_buffer_ptr);

		int sentinel = -1;
		this->curr_pos.push_back(sentinel);
		this->fd_index.push_back(sentinel);
		this->file_length.push_back(sentinel);

	}

	/*
	void* data = malloc(64);
	this->_oftfsm->iosys->read_block(2, (char*)data);
	this->_oftfsm->iosys->print_block_as_string((char*)data);
	free(data);
*/

	// this->print_oft();

}

FileSystem::OpenFileTable::~OpenFileTable()
{
	int oft_buffers_size = oft_buffers.size();
	for (int i = 0; i < oft_buffers_size; ++i)
	{
		free(oft_buffers[i]);
	}
}

void FileSystem::OpenFileTable::open_directory()
{
	this->_fsm->iosys->read_block(7, (char*)this->oft_buffers[0]);

	this->curr_pos[0] = 0;

	void* fd_index_ptr = (char*)this->oft_buffers[0] + sizeof(int);
	this->fd_index[0] = *(int*)fd_index_ptr;

	void* data = malloc(BLOCK_SIZE);
	int block_num = this->calc_fd_block_num(*(int*)fd_index_ptr);
	this->_fsm->iosys->read_block( block_num , (char*)data);
	int fd_block_offset = this->calc_fd_block_offset(*(int*)fd_index_ptr);
	this->file_length[0] = *(int*) ((char*)data + fd_block_offset * FILE_DESC_SIZE);
	free(data);

	if (printDebug && fsInitDebug)
		this->print_oft();
}

int FileSystem::OpenFileTable::get_current_pos(int oft_index)
{
	return this->curr_pos[oft_index];
}

void FileSystem::OpenFileTable::set_current_pos(int oft_index, int value)
{
	this->curr_pos[oft_index] = value;
}

void FileSystem::OpenFileTable::read_block_into_buffer(int file_block_num, int oft_index)
{
	int disk_block_num_of_file = this->get_disk_block_num_of_file(oft_index, file_block_num);
	if(printDebug && openDebug)
	{
		std::cout << "file_block_num = " << file_block_num << std::endl;
		std::cout << "disk_block_num_of_file = " << disk_block_num_of_file << std::endl;
	}
	this->_fsm->iosys->read_block(disk_block_num_of_file, (char*)this->oft_buffers[oft_index]);
}

int FileSystem::OpenFileTable::get_disk_block_num_of_file(int oft_index, int file_block_num)
{
	void* dir_block_data = malloc(BLOCK_SIZE);

	int fd_block_num = this->calc_fd_block_num(this->fd_index[oft_index]);
	this->_fsm->iosys->read_block(fd_block_num, (char*)dir_block_data);
	int fd_block_offset = this->calc_fd_block_offset(this->fd_index[oft_index]) * FILE_DESC_SIZE;
	int value_offset = sizeof(int) + file_block_num * sizeof(int);
	int disk_block_num_of_file = *(int*)((char*)dir_block_data + fd_block_offset + value_offset);
	if(printDebug && (readDebug || openDebug))
	{
		std::cout << "fd_block_num = " << fd_block_num << std::endl;
		std::cout << "fd_block_offset = " << fd_block_offset << std::endl;
		std::cout << "value_offset = " << value_offset << std::endl;
		std::cout << "FileSystem::OpenFileTable::get_disk_block_num_of_file = " << disk_block_num_of_file << std::endl;
	}

	free(dir_block_data);
	return disk_block_num_of_file;
}

//void FileSystem::OpenFileTable::set_file_length_in_fd(int fd_index, int file_length)
//{
//	void* dir_block_data = malloc(BLOCK_SIZE);
//
//	int fd_block_num = this->calc_fd_block_num(fd_index);
//	this->_fsm->iosys->read_block(fd_block_num, (char*)dir_block_data);
//	int fd_block_offset = this->calc_fd_block_offset(fd_index);
//	int value_offset = 0;
//	*(int*)((char*)dir_block_data + fd_block_offset + value_offset) = file_length;
//	this->_fsm->iosys->write_block(fd_block_num, (char*)dir_block_data);
//
//	free(dir_block_data);
//}

int FileSystem::OpenFileTable::find_free_oft_index()
{
	int free_oft_index = -1;
	for (int i = 0; i < OFT_TABLE_SIZE; ++i)
	{
		if (this->fd_index[i] == -1)
		{
			free_oft_index = i;
			break;
		}
	}

	return free_oft_index;
}

void FileSystem::OpenFileTable::print_oft()
{
	std::cout << "\ncur_pos\t\t" << "fd_index\t" << "file_len\t" << "Block" << std::endl;
	for (int i = 0; i < OFT_TABLE_SIZE; ++i)
	{

		std::cout << this->curr_pos[i] << "\t\t" << this->fd_index[i] << "\t\t" << this->file_length[i] << "\t\t";
		this->_fsm->iosys->print_block_as_string((char*)this->oft_buffers[i]);

	}
}

int FileSystem::OpenFileTable::calc_fd_block_num(int fd_index)
{
	int num_fds_per_block = BLOCK_SIZE/FILE_DESC_SIZE;
	int block_num = (fd_index / num_fds_per_block) + 1;
	return block_num;
}

int FileSystem::OpenFileTable::calc_fd_block_offset(int fd_index)
{
	int num_fds_per_block = BLOCK_SIZE/FILE_DESC_SIZE;
	int block_offset = fd_index % num_fds_per_block;
	return block_offset;
}





















