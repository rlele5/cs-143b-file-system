#include <iostream>
#include <cassert>
#include <ctime>
#include <fstream>
#include <vector>
#include <sstream>

#include "IOSystem.hpp"
#include "FileSystem.hpp"
#include "Driver.hpp"

bool printDebug = true;

bool fsInitDebug = false;
bool maskDebug = false;
bool lseekDebug = false;
bool readDebug = false;
bool writeDebug = false;
bool createDebug = false;
bool destroyDebug = false;
bool allocateDebug = false;
bool openDebug = false;
bool closeDebug = false;
bool dirDebug = false;

bool driverDebug = false;
bool gnDebug = false;
bool runDebug = false;
bool crDebug = false;
bool deDebug = false;
bool opDebug = false;
bool clDebug = false;
bool rdDebug = false;
bool wrDebug = false;
bool skDebug = false;
bool drDebug = false;
bool inDebug = false;
bool svDebug = false;


void FileSystemTesting();
void foo();
void foo2();
void DriverTesting();
void print_data_as_string(char* data, int print_length = BLOCK_SIZE);

int main() {

	std::cout << "Begin main\n-------------\n" << std::endl;

	//FileSystemTesting();
	//foo();
	DriverTesting();
	//foo2();


//	std::string str= "rahul";
//	std::cout << "str.size() = " << str.size() << std::endl;
//	std::cout << "std::strlen('rahul') = " << std::strlen(str.c_str()) << std::endl;

	std::cout << "\n-------------\nEnd main" << std::endl;
	return 0;
}

void foo2()
{
	std::string str = "123";
	int num = std::stoi(str, nullptr, 10);
	std::cout << "num = " << num << std::endl;
	std::cout << "num+4 = " << num+4 << std::endl;
}

void DriverTesting()
{
	Driver* driver = new Driver("asdf.txt");
	driver->run();

	delete driver;


}

void foo()
{

	std::ifstream myReadFile;
	myReadFile.open("test-sample.txt");



	while(true)
	{
		std::string line;
		std::vector<std::string> frags;
		std::getline(myReadFile, line);
		if (myReadFile.eof())
			break;

		std::cout << "line = " << line;
		std::istringstream iline(line);
		while(true)
		{
			std::string temp;
			std::getline(iline, temp, ' ');
			if (!iline)
				break;

			frags.push_back(temp);
		}

		for (int i = 0; i < frags.size(); ++i)
		{
			std::cout << "\t\t" << frags[i];
		}
		std::cout << std::endl;


	}

	myReadFile.close();


//	FILE* pFile;
//	pFile = fopen("test-sample.txt", "r");
//	if (pFile == NULL)
//	{
//		std::cout << "pFile NULL" << std::endl;
//		return;
//	}
////	pFile = fopen(disk_filename.c_str(), "r");
////	if (pFile == NULL)
////		return rc_ldisk_file_does_not_exist;
////
////	fread(this->ldisk, 1, LDISK_SIZE, pFile);
//	fclose(pFile);


}

void FileSystemTesting()
{
	FileSystem* filesys = FileSystem::instance();
	filesys->init();
	RC rc;

	std::cout << "\n%%%%%%%%%%%%%%%% Read test: %%%%%%%%%%%%%%%%\n" << std::endl;
	void* data = malloc(BLOCK_SIZE*3);
	memset(data, '*', BLOCK_SIZE*3);
	rc = filesys->read(0, data, 160);
	print_data_as_string((char*)data, 192);
	free(data);
	std::cout << "\n%%%%%%%%%%%%%%%% END Read test %%%%%%%%%%%%%%%% \n " << std::endl;

	std::string filename = "abc";
	filesys->create(filename);
	filesys->create("def");
	filesys->create("ghi");
	int oft_index = filesys->open("def");

	void* temp = malloc(192);
	void* data2 = malloc(192);
	memset(temp, '*', BLOCK_SIZE*3);
	memset(data2, '+', BLOCK_SIZE*3);
	memset(temp, '^', 128);
//	filesys->lseek(oft_index, 16);
	std::cout << "WRITE 1: " << std::endl;
	int written = filesys->write(oft_index, temp, 64);
	std::cout << "WRITE 2: " << std::endl;
	written += filesys->write(oft_index, temp, 64);
	std::cout << "WRITE 3: " << std::endl;
	written += filesys->write(oft_index, temp, 68);
	filesys->lseek(oft_index, 0);
	int read = filesys->read(oft_index, data2, 128);
	std::cout << "written = " << written << "\tread = " << read << std::endl;
	print_data_as_string((char*)data2, 192);

	filesys->close(1);
	filesys->ldisk_export("test2.txt");
//	filesys->destroy("def");

	filesys->save("save_disk.txt");
	filesys->ldisk_export("test_save.txt");
	filesys->init("save_disk.txt");

	oft_index = filesys->open("def");
	filesys->lseek(oft_index, 0);
	memset(data2, '*', 192);
	read = filesys->read(oft_index, data2, 128);
	std::cout /*<< "written = " << written */ << "\tread = " << read << std::endl;
	print_data_as_string((char*)data2, 192);


//	filesys->create("hij");
//	int oft_index2 = filesys->open("ghi");
//	std::cout << "WRITE 4: " << std::endl;
//	memset(temp, ')', 192);
//	int written2 = filesys->write(oft_index2, temp, 68);


//	std::cout << "\n%%%%%%%%%%%%%%%% Lseek test: %%%%%%%%%%%%%%%%" << std::endl;
//	filesys->lseek(0, 16);
//	std::cout << "\n%%%%%%%%%%%%%%%% END Lseek test %%%%%%%%%%%%%%%% \n " << std::endl;
//
//	std::cout << "\n%%%%%%%%%%%%%%%% Simple Write test: %%%%%%%%%%%%%%%%\n" << std::endl;
//	void* write_data = malloc(BLOCK_SIZE * 3);
//	memset(write_data, '#', BLOCK_SIZE*3);
//	filesys->write(0, (char*)write_data, 128);
//	std::cout << "\n%%%%%%%%%%%%%%%% END Simple Write test %%%%%%%%%%%%%%%% \n " << std::endl;
//
//	std::cout << "\n%%%%%%%%%%%%%%%% Invalid lseek then Write test: %%%%%%%%%%%%%%%%\n" << std::endl;
//	int returned_pos =  filesys->lseek(0, 180);
//	filesys->write(0, (char*)write_data, 32);
//	free(write_data);
//	std::cout << "\n%%%%%%%%%%%%%%%% END Invalid lseek then Write test %%%%%%%%%%%%%%%% \n" << std::endl;

	//std::cout << filesys->find_free_block_on_ldisk() << std::endl;

	free(temp);
	free(data2);
	filesys->ldisk_export("test3.txt");

}

void print_data_as_string(char* data, int print_length)
{
	std::string block_string = std::string (data, print_length);
	std::cout << "String (" << print_length << ")" << " : " << block_string << std::endl;
}

















//	RC rc;
//	IOSystem iosys;
//	rc = iosys.import_disk("test_ldisk.txt");
//
//	void* data = malloc(BLOCK_SIZE);
//	iosys.read_block(0, (char*)data);
//	iosys.print_block_as_string( (char*)data);
//
//	for (char i = 0; i < 64; ++i)
//	{
//		*((char*)data + i) = i % 26 + 65;
//	}
//	iosys.print_block_as_string( (char*)data);
//
//	iosys.write_block(1, (char*)data);
//	rc = iosys.export_disk("test_ldisk2.txt");
