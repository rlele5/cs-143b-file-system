/*
 * IOSystem.hpp
 *
 *  Created on: Jan 8, 2015
 *      Author: Rahul
 */

#ifndef IOSYSTEM_HPP_
#define IOSYSTEM_HPP_

#include <string>
#include <cstdio>
//#include <memory>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <cstring>

#define BLOCK_SIZE 64
#define NUM_BLOCKS 64
#define LDISK_SIZE NUM_BLOCKS*BLOCK_SIZE

#define rc_success 0
#define rc_ldisk_file_does_not_exist -1
#define rc_invalid_address -2
#define rc_invalid_read_request -1
#define rc_invalid_write_request -1
#define rc_filename_too_big -1
#define rc_invalid_seek_request -1
#define rc_failed_alloc -1
#define rc_file_does_not_exist -1
#define rc_oft_full -1
#define rc_filename_already_exists -1
#define rc_trying_to_delete_open_file -1

extern bool printDebug;

typedef int RC;

class IOSystem
{

public:
	IOSystem();
	~IOSystem();
	RC read_block(int block_num, char* data);
	RC write_block(int block_num, char* data);

	RC import_disk(std::string disk_filename); // Reading and writing logical disks to files for persistent storage
	RC export_disk(std::string disk_filename);

	void print_block_as_string(char* data);
//	void print_data_as_string(char* data, int print_length = BLOCK_SIZE);

private:
	void* ldisk;

};



#endif /* IOSYSTEM_HPP_ */
