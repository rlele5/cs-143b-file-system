/*
 * FileSystem.hpp
 *
 *  Created on: Jan 10, 2015
 *      Author: Rahul
 */

#ifndef FILESYSTEM_HPP_
#define FILESYSTEM_HPP_

#include <string>
#include <vector>
#include <bitset>
#include <cassert>

#include "IOSystem.hpp"

extern bool printDebug;
extern bool fsInitDebug;
extern bool maskDebug;
extern bool lseekDebug;
extern bool readDebug;
extern bool writeDebug;
extern bool createDebug;
extern bool destroyDebug;
extern bool allocateDebug;
extern bool openDebug;
extern bool closeDebug;
extern bool dirDebug;


#define INIT_BITMAP_ALLOCATION 10
#define BITMAP_NUM_INTS 2
#define DIR_ENTRY_SIZE 8
#define FILE_DESC_SIZE 16
#define OFT_TABLE_SIZE 4
#define FDS_PER_BLOCK BLOCK_SIZE/FILE_DESC_SIZE
//#define OFT_ENTRY_SIZE
#define FILE_SIZE_BLOCKS 3
#define FIRST_DIRECTORY_BLOCK 7
#define LAST_DIRECTORY_BLOCK 9
#define FIRST_FD_BLOCK 1
#define LAST_FD_BLOCK 6
#define MAX_FILENAME_SIZE 3

#define OFT_DIR 0
#define DIR_ENTRIES_PER_BLOCK BLOCK_SIZE/DIR_ENTRY_SIZE
#define FILENAME_CHUNK 4

typedef int mask_size;

class FileSystem
{
	class OpenFileTable;

protected:

	FileSystem();
	~FileSystem();

public:
	static FileSystem* instance();

	RC create(std::string filename);
	RC destroy(std::string filename);
	int open(std::string filename);
	RC close(int oft_index);
	int read(int oft_index, void* read_data, int count);
	int write(int oft_index, void* write_data, int count);
	RC lseek(int oft_index, int pos);
	std::vector<std::string> directory();

	RC init();
	RC init(std::string disk_name);
	RC save(std::string disk_name);

private:
	static FileSystem* _file_system_manager;
	IOSystem* iosys;
	unsigned int MASK[32];
	unsigned int MASK2[32];
	OpenFileTable* _oft = NULL; // To be opened and closed in init and save
	//void* read_data_buffer;

	void init_masks();
	void set_bit(int &num, int i);
	void clr_bit(int &num, int i);
	int get_bit(const int &num, int i);
	void set_bitmap(int block_num);
	void clr_bitmap(int block_num);

	void init_file_descriptor_block(void* fd_data);
	void init_directory_block(void* dir_data);
	int calc_file_block_num(int file_pos);
	//int calc_num_ints_bitmap(int num_blocks_on_ldisk);
	int find_free_block_on_ldisk();
	int find_free_fd_index();
	void create_fd_entry(void* fd_data, int file_length, int disk_block_index_0, int disk_block_index_1, int disk_block_index_2);
	void set_fd_entry(int fd_index, void* fd_data);
	void get_fd_entry(int fd_index, void* fd_data);
	void print_fd_entry(int fd_index, void* fd_data);
	int get_fd_value(int fd_index, int int_index);
	void set_fd_value(int fd_index, int int_index, int value);
	RC allocate_block(int fd_index);

	int find_free_directory_entry();
	void create_directory_entry(void* dir_entry_data, std::string filename, int fd_index);
	void set_directory_entry(int dir_entry_index, void* dir_entry_data);
	void get_directory_entry(int dir_entry_index, void* dir_entry_data);
	//void print_directory_entry(int dir_entry_index, void* dir_entry_data);

public:
	void ldisk_export(std::string disk_name);


private:

	class OpenFileTable
	{
	public:
		OpenFileTable(FileSystem* _file_system_manager);
		~OpenFileTable();

		void open_directory();
		int get_current_pos(int oft_index);
		void set_current_pos(int oft_index, int value);
		void read_block_into_buffer(int file_block_num, int oft_index);
		int get_disk_block_num_of_file(int oft_index, int file_block_num);
//		void set_file_length_in_fd(int fd_index, int file_length);
		int calc_fd_block_num(int fd_index);
		int calc_fd_block_offset(int fd_index);

		int find_free_oft_index();

		void print_oft();
		std::vector<void*> oft_buffers;
		std::vector<int> curr_pos;
		std::vector<int> fd_index;
		std::vector<int> file_length;

	private:
		FileSystem* _fsm;




	};


};



#endif /* FILESYSTEM_HPP_ */
