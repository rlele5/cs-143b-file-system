/*
 * Driver.hpp
 *
 *  Created on: Jan 25, 2015
 *      Author: rahul
 */

#ifndef DRIVER_HPP_
#define DRIVER_HPP_

#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

#include "IOSystem.hpp"
#include "FileSystem.hpp"

extern bool printDebug;
extern bool driverDebug;
extern bool gnDebug;
extern bool runDebug;

extern bool crDebug;
extern bool deDebug;
extern bool opDebug;
extern bool clDebug;
extern bool rdDebug;
extern bool wrDebug;
extern bool skDebug;
extern bool drDebug;
extern bool inDebug;
extern bool svDebug;

class Driver
{

public:

	Driver(std::string filename);
	~Driver();

	void run();
	RC getNext();


//private:
	FileSystem* filesys;
	std::string line;
	std::vector<std::string> frags;
	std::ifstream inputFile;
	std::ofstream outputFile;

	void cr();
	void de();
	void op();
	void cl();
	void rd();
	void wr();
	void sk();
	void dr();
	void in();
	void sv();




};






#endif /* DRIVER_HPP_ */
