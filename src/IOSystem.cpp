/*
 * IOSystem.cpp
 *
 *  Created on: Jan 8, 2015
 *      Author: Rahul
 */


#include "IOSystem.hpp"

IOSystem::IOSystem()
{
	this->ldisk = malloc(LDISK_SIZE);
	memset( this->ldisk, '~', LDISK_SIZE);

}

IOSystem::~IOSystem()
{
	free(this->ldisk);
}

RC IOSystem::read_block(int block_num, char* data)
{
	unsigned int byteOffset = block_num * BLOCK_SIZE;
	if (byteOffset > LDISK_SIZE - BLOCK_SIZE)
	{
		std::cout << "\nInvalid address - IOSystem::read_block" << std::endl;
		std::cout << "byteOffset = " << byteOffset << std::endl;
		std::cout << "block_num = " << block_num << std::endl;
		assert( !(byteOffset > LDISK_SIZE - BLOCK_SIZE) );
		return rc_invalid_address; // For when assert is removed
	}
	memcpy(data, (char*)ldisk + byteOffset, BLOCK_SIZE);
	return rc_success;
}
RC IOSystem::write_block(int block_num, char* data)
{
	unsigned int byteOffset = block_num * BLOCK_SIZE;
	if (byteOffset > LDISK_SIZE - BLOCK_SIZE)
	{
		std::cout << "Invalid address - IOSystem::write_block" << std::endl;
		assert( !(byteOffset > LDISK_SIZE - BLOCK_SIZE) );
		return rc_invalid_address;
	}
	memcpy((char*)ldisk + byteOffset, data, BLOCK_SIZE);
	return rc_success;
}

RC IOSystem::import_disk(std::string disk_filename)
{
	FILE* pFile;
	pFile = fopen(disk_filename.c_str(), "r");
	if (pFile == NULL)
		return rc_ldisk_file_does_not_exist;

	fread(this->ldisk, 1, LDISK_SIZE, pFile);
	fclose(pFile);
	return rc_success;
}

RC IOSystem::export_disk(std::string disk_filename)
{
	FILE* pFile;
	pFile = fopen(disk_filename.c_str(), "w");
	fwrite(this->ldisk, 1, LDISK_SIZE, pFile);
	fclose(pFile);
	//memset(this->ldisk, '=', LDISK_SIZE);
	return rc_success;
}

void IOSystem::print_block_as_string(char* data)
{
	std::string block_string = std::string (data, BLOCK_SIZE);
	std::cout << "Block string:  " << block_string << std::endl;
}
