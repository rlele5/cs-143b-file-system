/*
 * Driver.cpp
 *
 *  Created on: Jan 25, 2015
 *      Author: rahul
 */


#include "Driver.hpp"


Driver::Driver(std::string filename)
{
	filesys = FileSystem::instance();
	this->inputFile.open(filename.c_str());
	if ( this->inputFile == NULL )
		{
			std::cout << "No input file by this name\n" << std::endl;
		}
	std::string outputFilename = "output_";
	outputFilename.append(filename);
	outputFilename = "42365632.txt";
	this->outputFile.open(outputFilename.c_str());

}

Driver::~Driver()
{
	this->inputFile.close();
	this->outputFile.close();
	filesys->ldisk_export("driver_end.txt");
}

void Driver::run()
{
	std::cout << "Please press \"Enter\" after every command:" << std::endl;
	RC rc;
	while(true)
	{
//		std::cin.ignore();
		rc = this->getNext();

		if (printDebug && runDebug)
		{
			//std::cout << "\tfrags[0] = " << frags[0] << std::endl;
			std::cout << "frags[0].size = " << this->frags[0].size() << std::endl;
			int vecsize = this->frags.size();
			std::cout << "vecsize = " << vecsize << std::endl;
			for(int i = 0; i < vecsize; ++i)
			{
				int strsize = this->frags[i].size();
				for (int j = 0; j < strsize; ++j)
				{
					std::cout << 0 + frags[i][j] << " ";
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
		}

		if (this->frags[0] == "cr")
		{
			this->cr();
		}
		else if (this->frags[0] == "de")
		{
			this->de();
		}
		else if (this->frags[0] == "op")
		{
			this->op();
		}
		else if (this->frags[0] == "cl")
		{
			this->cl();
		}
		else if (this->frags[0] == "rd")
		{
			this->rd();
		}
		else if (this->frags[0] == "wr")
		{
			this->wr();
		}
		else if (this->frags[0] == "sk")
		{
			this->sk();
		}
		else if (this->frags[0] == "dr")
		{
			this->dr();
		}
		else if (this->frags[0] == "in")
		{
			this->in();
		}
		else if (this->frags[0] == "sv")
		{
			this->sv();
		}
		else
		{
			//std::cout << "\t\t\t\t:\t##### ERROR: invalid command #####";
			this->outputFile << "\n";
		}

		if (rc == -1)
				break;

	}

}

RC Driver::getNext()
{
//	if (printDebug && gnDebug)
//		std::cout << "------ ENTER GETNEXT ------" << std::endl;

	std::vector<std::string> temp_frags;

	std::getline(inputFile, this->line);
	//std::cout << "\n\t--" << this->line << std::endl;

	std::istringstream iline(this->line);
	while(true)
	{
		std::string temp;
		std::getline(iline, temp, ' ');
		if (!iline)
			break;

		/* Get rid of non alphanumeric characters at end of string. Gets rid of ASCII = 13 */
		char ch = temp.back();
		if ( ch < 48 || (ch > 57 && ch < 65) || (ch > 90 && ch < 97) || (ch > 122))
		{
			//std::cout << "hit = " << 0 + ch << std::endl;
			temp.pop_back();
		}

		temp_frags.push_back(temp);
	}

	this->frags = temp_frags; // Would be better to use references, but whatever

	int size = this->frags.size();
	std::cout << std::endl;
	if ( /*printDebug && gnDebug || */ true)
	{
		for (int i = 0; i < size; ++i)
		{
			std::cout << "\t" << this->frags[i];
		}
	}

//	if (printDebug && gnDebug)
//		std::cout << "------ EXIT GETNEXT ------" << std::endl;

	if (inputFile.eof())
		return -1;
	else
		return rc_success;
}


void Driver::cr()
{
	if (printDebug && crDebug)
	{
		std::cout << "------ ENTER CR ------" << std::endl;
	}

	RC rc = this->filesys->create(frags[1]);
	if (rc == rc_success)
	{
		std::cout << "\t\t\t:\t" << frags[1] << " created";
		this->outputFile << frags[1] << " created\n";
	}
	else
	{
		std::cout << "\t\t\t:\terror";
		this->outputFile << "error\n";
	}

	if (printDebug && crDebug)
	{
		std::cout << "------ EXIT CR ------\n\n" << std::endl;
	}

}

void Driver::de()
{
	if (printDebug && deDebug)
	{
		std::cout << "------ ENTER DE ------" << std::endl;
	}

	std::string filename = frags[1];
	RC rc = this->filesys->destroy(filename);
	if (rc == rc_success)
	{
		std::cout << "\t\t\t:\t" << frags[1] << " destroyed";
		this->outputFile << frags[1] << " destroyed\n";
	}
	else
	{
		std::cout << "\t\t\t:\terror";
		this->outputFile << "error\n";
	}

	if (printDebug && deDebug)
	{
		std::cout << "------ EXIT DE ------\n\n" << std::endl;
	}
}

void Driver::op()
{
	if (printDebug && opDebug)
	{
		std::cout << "------ ENTER OP ------" << std::endl;
	}

	int oft_index = this->filesys->open(frags[1]);
	if (oft_index >= 0)
	{
		std::cout << "\t\t\t:\t"<< frags[1] << " opened " << oft_index;
		this->outputFile << frags[1] << " opened\n";
	}
	else
	{
		std::cout << "\t\t\t:\terror";
		this->outputFile << "error\n";
	}


	if (printDebug && opDebug)
	{
		std::cout << "------ EXIT OP ------\n\n" << std::endl;
	}
}

void Driver::cl()
{
	if (printDebug && clDebug)
	{
		std::cout << "------ ENTER CL ------" << std::endl;
	}

	int oft_index = std::stoi(frags[1], nullptr, 10);
	RC rc = this->filesys->close(oft_index);

	if (rc == rc_success)
	{
		std::cout << "\t\t\t:\t" << oft_index << " closed";
		this->outputFile << oft_index << " closed\n";
	}
	else
	{
		std::cout << "\t\t\t:\t" << "error";
		this->outputFile << "error\n";
	}

	if (printDebug && clDebug)
	{
		std::cout << "------ EXIT CL ------\n\n" << std::endl;
	}
}

void Driver::rd()
{
	if (printDebug && rdDebug)
	{
		std::cout << "------ ENTER RD ------" << std::endl;
	}

	int oft_index = std::stoi(frags[1], nullptr, 10);
	int length_to_read = std::stoi(frags[2], nullptr, 10);
	if (length_to_read > 192)
	{
		length_to_read = 192;
	}

	void* read_data = malloc(BLOCK_SIZE*FILE_SIZE_BLOCKS);
	memset(read_data, 'R', BLOCK_SIZE*FILE_SIZE_BLOCKS);
	int num_read = this->filesys->read(oft_index, read_data, length_to_read);
	std::string read_data_str = std::string((char*)read_data, num_read);
	std::cout << "\t\t:\t" << read_data_str;
	this->outputFile << read_data_str << "\n";
	free(read_data);

	if (printDebug && rdDebug)
	{
		std::cout << "------ EXIT RD ------\n\n" << std::endl;
	}
}

void Driver::wr()
{
	if (printDebug && wrDebug)
	{
		std::cout << "------ ENTER WR ------" << std::endl;
	}

	int oft_index = std::stoi(frags[1], nullptr, 10);
	char character = frags[2].front();
	int length_to_write = std::stoi(frags[3], nullptr, 10);
	if (length_to_write > 192)
	{
		length_to_write = 192;
	}

	void* write_data = malloc(BLOCK_SIZE*FILE_SIZE_BLOCKS);
	memset(write_data, 'W', BLOCK_SIZE*FILE_SIZE_BLOCKS);
	memset(write_data, character, length_to_write);
	int bytes_written = this->filesys->write(oft_index, write_data, length_to_write);
	free(write_data);

	std::cout << "\t:\t" << bytes_written << " bytes written";
	this->outputFile << bytes_written << " bytes written\n";

	if (printDebug && wrDebug)
	{
		std::cout << "------ EXIT WR ------\n\n" << std::endl;
	}
}

void Driver::sk()
{
	if (printDebug && skDebug)
	{
		std::cout << "------ ENTER SK ------" << std::endl;
	}

	int oft_index = std::stoi(frags[1], nullptr, 10);
	int seek_position = std::stoi(frags[2], nullptr, 10);
	RC rc = this->filesys->lseek(oft_index, seek_position);

	if (rc == rc_success)
	{
		std::cout << "\t\t:\tposition is " << seek_position;
		this->outputFile << "position is " << seek_position << "\n";
	}
	else
	{
		std::cout << "\t\t:\terror";
		this->outputFile << "error\n";
	}

	if (printDebug && skDebug)
	{
		std::cout << "------ EXIT SK ------\n\n" << std::endl;
	}
}

void Driver::dr()
{
	if (printDebug && drDebug)
	{
		std::cout << "------ ENTER DR ------" << std::endl;
	}

	std::vector<std::string> filenames = this->filesys->directory();
	std::cout << "\t\t\t\t:";
	int size = filenames.size();
	for (int i = 0; i < size; ++i)
	{
		std::cout << "\t" << filenames[i];
		this->outputFile << filenames[i] << " ";
	}
	this->outputFile << "\n";

	if (printDebug && drDebug)
	{
		std::cout << "------ EXIT DR ------\n\n" << std::endl;
	}
}

void Driver::in()
{
	if (printDebug && inDebug)
	{
		std::cout << "------ ENTER IN ------" << std::endl;
	}

	if (frags.size() == 1)
	{
		if (printDebug && inDebug)
		{
			std::cout << "\t\tin: case 1" << std::endl;
		}
		this->filesys->init(); // Fresh init
		this->filesys->ldisk_export("after_init.txt");
		std::cout << "\t\t\t\t:\tdisk initialized";
		this->outputFile << "disk initialized\n";
	}
	else if (frags.size() == 2)
	{
		if (printDebug && inDebug)
		{
			std::cout << "\t\tin: case 2: filename = " << frags[1] << std::endl;
		}
		this->filesys->init(frags[1]); // Init from disk0.txt
		std::cout << "\t\t:\tdisk restored";
		this->outputFile << "disk restored\n";
	}

	if (printDebug && inDebug)
	{
		std::cout << "------ EXIT IN ------\n\n" << std::endl;
	}
}

void Driver::sv()
{
	if (printDebug && svDebug)
	{
		std::cout << "------ ENTER SV ------" << std::endl;
	}

	RC rc = this->filesys->save(frags[1]);
	if (rc == rc_success)
	{
		std::cout << "\t\t:\tdisk saved";
		this->outputFile << "disk saved\n";
	}
	else
	{
		std::cout << "\t\t:\terror";
		this->outputFile << "error\n";
	}


	if (printDebug && svDebug)
	{
		std::cout << "------ EXIT SV ------\n\n" << std::endl;
	}
}
